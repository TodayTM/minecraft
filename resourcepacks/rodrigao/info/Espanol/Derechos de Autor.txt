
Infringir las normas ocasionar� reclamaci�n por derechos de autor y retiro
de dicho contenido. A continuaci�n ver�s las reglas del paquete de texturas:

-------------------------------------------------------------------------------------------------------------------

� Puedes:

     - Usar mis paquetes de texturas para videos y transmisiones con o sin cr�ditos

     - Hacer edits, mashups o revamps de mis texturas solo para uso personal/privado
       sin distribuci�n, comercializaci�n o monetizaci�n

     - Hacer reviews en videos, foros y p�ginas webs dejando necesariamente todos
       los cr�ditos de abajo, sin cambios o exclusiones de alguno de ellos

     * Nombre: (8x8) Rodrigo's Pack
         Creador: https://twitter.com/Rodrigo_Al_
     * Descarga: https://rodrigo-al.jimdo.com/texture-packs/8x8-rodrigo-s-pack/ 

         Cr�ditos con un asterisco (*) son totalmente necesarios

-------------------------------------------------------------------------------------------------------------------

� No puedes:

     - Usar mis texturas para hacer edits, remixes o mashups de �stos para 
       distribuirlos, comercializarlos o monetizarlos

     - Usar  enlaces de almacenamiento externo en la nube como MediaFire
       o MEGA para distribuir mis paquetes de texturas

     - Usar mis texturas para incluirlas en paquetes de mods o carpetas de paquetes
       de texturas para distribuirlos, comercializarlos o monetizarlos

     - Hacer reviews de mis paquetes de texturas en videos, foros y p�ginas web sin
       ning�n tipo de cr�ditos como previamente se describi� en los ejemplos de cr�ditos

     - Portar mis paquetes de texturas a otras versiones de juego como Bedrock o Minetest

-------------------------------------------------------------------------------------------------------------------

� Licencia detallada:

     - https://rodrigo-al.jimdo.com/license/

� Licencia legal:

     - https://creativecommons.org/licenses/by-nc-nd/4.0/

-------------------------------------------------------------------------------------------------------------------

                                              Rodrigo's Pack, 2016 - 2020 �