
Thanks for download my texture pack, I hope you enjoy it! I'm constantly updating it, 
so I recommend to you check at least once a month the link at bottom to keep it updated

-------------------------------------------------------------------------------------------------------------------

� Follow me:

     - Twitter: https://twitter.com/Rodrigo_Al_
     - YouTube: https://youtube.com/c/Rodrigo_Al
     - My Discord server: https://discord.gg/Z4v2tky
     - Contact: https://rodrigo-al.jimdofree.com/contact/

-------------------------------------------------------------------------------------------------------------------

� Basic information: 

     - Author: Rodrigo Al.
     - Name: Rodrigo's Pack (8x8)
     - Creation: July 16, 2016

-------------------------------------------------------------------------------------------------------------------

� Update texture pack:

     - https://www.planetminecraft.com/texture_pack/8x8-rodrigos-pack-pvp/

� Donations:

     - https://rodrigo-al.jimdofree.com/donate/

-------------------------------------------------------------------------------------------------------------------

                                              Rodrigo's Pack, 2016 - 2020 �